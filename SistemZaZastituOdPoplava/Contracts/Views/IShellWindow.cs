﻿using System.Windows.Controls;

namespace SistemZaZastituOdPoplava.Contracts.Views
{
    public interface IShellWindow
    {
        Frame GetNavigationFrame();

        void ShowWindow();

        void CloseWindow();
    }
}
