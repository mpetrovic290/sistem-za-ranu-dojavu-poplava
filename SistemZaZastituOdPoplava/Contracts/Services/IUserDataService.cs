﻿using System;

using SistemZaZastituOdPoplava.Models;

namespace SistemZaZastituOdPoplava.Contracts.Services
{
    public interface IUserDataService
    {
        event EventHandler<UserData> UserDataUpdated;

        void Initialize();

        UserData GetUser();
    }
}
