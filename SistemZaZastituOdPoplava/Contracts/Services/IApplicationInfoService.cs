﻿using System;

namespace SistemZaZastituOdPoplava.Contracts.Services
{
    public interface IApplicationInfoService
    {
        Version GetVersion();
    }
}
