﻿using System;

using SistemZaZastituOdPoplava.Models;

namespace SistemZaZastituOdPoplava.Contracts.Services
{
    public interface IThemeSelectorService
    {
        void InitializeTheme();

        void SetTheme(AppTheme theme);

        AppTheme GetCurrentTheme();
    }
}
