﻿using System;
using System.Windows.Media.Imaging;

namespace SistemZaZastituOdPoplava.Models
{
    public class UserData
    {
        public string Name { get; set; }

        public string UserPrincipalName { get; set; }

        public BitmapImage Photo { get; set; }
    }
}
