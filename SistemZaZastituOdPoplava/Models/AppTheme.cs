﻿namespace SistemZaZastituOdPoplava.Models
{
    public enum AppTheme
    {
        Default,
        Light,
        Dark
    }
}
