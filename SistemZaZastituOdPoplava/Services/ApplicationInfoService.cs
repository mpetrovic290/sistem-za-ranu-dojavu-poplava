﻿using System;
using System.Diagnostics;
using System.Reflection;

using SistemZaZastituOdPoplava.Contracts.Services;

namespace SistemZaZastituOdPoplava.Services
{
    public class ApplicationInfoService : IApplicationInfoService
    {
        public ApplicationInfoService()
        {
        }

        public Version GetVersion()
        {
            // Set the app version in SistemZaZastituOdPoplava > Properties > Package > PackageVersion
            string assemblyLocation = Assembly.GetExecutingAssembly().Location;
            var version = FileVersionInfo.GetVersionInfo(assemblyLocation).FileVersion;
            return new Version(version);
        }
    }
}
