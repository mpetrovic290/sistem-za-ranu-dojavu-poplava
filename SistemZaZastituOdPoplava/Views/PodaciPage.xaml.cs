﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Windows.Controls;

using SistemZaZastituOdPoplava.Contracts.Views;
using SistemZaZastituOdPoplava.Core.Contracts.Services;
using SistemZaZastituOdPoplava.Core.Models;

namespace SistemZaZastituOdPoplava.Views
{
    public partial class PodaciPage : Page, INotifyPropertyChanged, INavigationAware
    {
        private readonly ISampleDataService _sampleDataService;

        public ObservableCollection<SampleOrder> Source { get; } = new ObservableCollection<SampleOrder>();

        public PodaciPage(ISampleDataService sampleDataService)
        {
            _sampleDataService = sampleDataService;
            InitializeComponent();
            DataContext = this;
        }

        public async void OnNavigatedTo(object parameter)
        {
            Source.Clear();

            // TODO WTS: Replace this with your actual data
            var data = await _sampleDataService.GetGridDataAsync();

            List<StanjeReke> sr = new List<StanjeReke>();
            var json = new WebClient().DownloadString("https://pastebin.com/raw/Jt2C90Fb");
            sr = JsonSerializer.Deserialize<List<StanjeReke>>(json);

            foreach(StanjeReke s in sr)
                DataGridTest.Items.Add(s);
        }

        public void OnNavigatedFrom()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
