﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemZaZastituOdPoplava.Core.Models
{
    public class StanjeReke
    {
        public String Reka { get; set; }
        public String Hid_Stanica { get; set; }
        public int Vodostaj { get; set; }
        public int GRO { get; set; }
        public int GVO { get; set; }
    }
}
