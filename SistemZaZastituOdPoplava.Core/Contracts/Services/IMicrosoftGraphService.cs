﻿using System.Threading.Tasks;

using SistemZaZastituOdPoplava.Core.Models;

namespace SistemZaZastituOdPoplava.Core.Contracts.Services
{
    public interface IMicrosoftGraphService
    {
        Task<User> GetUserInfoAsync(string accessToken);

        Task<string> GetUserPhoto(string accessToken);
    }
}
