﻿using System.Collections.Generic;
using System.Threading.Tasks;

using SistemZaZastituOdPoplava.Core.Models;

namespace SistemZaZastituOdPoplava.Core.Contracts.Services
{
    public interface ISampleDataService
    {
        Task<IEnumerable<SampleOrder>> GetMasterDetailDataAsync();

        Task<IEnumerable<SampleOrder>> GetGridDataAsync();
    }
}
